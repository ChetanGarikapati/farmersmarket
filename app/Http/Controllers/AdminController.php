<?php

namespace App\Http\Controllers;

use App\Models\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    private static $ADMIN_ROLE = "admin";

    public function validateUserAdminRole(Request $request)
    {
        if (!Auth::check()) {
            return redirect("/login");
        }
        if (!$this->checkAdminPrivileges())
            return redirect("/dashboard");
        return View("admin");
    }

    public static function checkAdminPrivileges()
    {

        $userRoles = UserRole::where('user_id', Auth::user()->id)->first();
        if ($userRoles != null && strcmp($userRoles->role, AdminController::$ADMIN_ROLE) == 0)
            return true;
        return false;

    }
}
