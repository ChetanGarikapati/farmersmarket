<?php

namespace App\Http\Controllers;

use App\Models\OrderItems;
use App\Models\Orders;
use Illuminate\Http\Request;
use stdClass;

class AdminOrderMangementController extends Controller
{
    public function manageOrders(Request $request)
    {
        if (!AdminController::checkAdminPrivileges()) {
            return redirect("/dashboard");
        }

        $orderInfo = new stdClass();

        if ($request->has("userid")) {
            $orderInfo->orderData = Orders::where('user_id', $request->get("userid"))->get();
            return response()->json($orderInfo);
        } elseif ($request->has("orderid")) {
            $orderInfo->orderData = Orders::where("id", $request->get("orderid"))->get();
            $orderInfo->orderItemData = OrderItems::where('order_id', $request->get("orderid"))->get();
            return response()->json($orderInfo);
        }

        return View("manageorder");
    }

    public function updateOrderStatus(Request $request)
    {
        if (!AdminController::checkAdminPrivileges()) {
            return redirect("/dashboard");
        }

        if ($request->has("updated-status")) {
            $order = Orders::where('id', $request->get("orderid"))->first();

            if ($order != null) {
                $orderItems = OrderItems::where('order_id', $request->get("orderid"))->get();
                foreach ($orderItems as $orderItem) {
                    $orderItem->order_item_status = $request->get("updated-status");
                    $orderItem->save();
                }
                $order->order_status = $request->get("updated-status");
                $order->save();
            }

            return response("Order status updated");
        }

        return response("No order was found to update");
    }
}
