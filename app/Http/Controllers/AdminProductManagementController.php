<?php

namespace App\Http\Controllers;

use App\Models\Catalog;
use App\Models\Prices;
use Illuminate\Http\Request;
use stdClass;

class AdminProductManagementController extends Controller
{
    public function manageProducts()
    {
        if (!AdminController::checkAdminPrivileges()) {
            return redirect("/dashboard");
        }

        return View("manageproducts");
    }

    function store(Request $request)
    {
        $productInfo = new stdClass();

        $newProduct = new Catalog;
        $newProduct->product_name = $request->get("product_name");
        $newProduct->product_type = $request->get("product_type");
        $newProduct->quantity = $request->get("quantity");
        $newProduct->deleted = $request->get("deleted");
        $newProduct->product_description = $request->get("product_description");
        $newProduct->image_name = $request->get("product_image");
        $newProduct->save();

        $price = new Prices();
        $price->product_name = $newProduct->product_name;
        $price->mrp = 5;
        $price->catalog_id = $newProduct->id;
        $price->save();

        $temp = array();
        array_push($temp, $newProduct);
        $productInfo->productData = $temp;
        return response()->json($productInfo);
    }

    function update(Request $request)
    {
        $productInfo = new stdClass();

        $updateProduct = Catalog::where('id', $request->get("productid"))->first();
        if ($updateProduct != null) {
            $updateProduct->product_name = $request->get("product_name");
            $updateProduct->product_type = $request->get("product_type");
            $updateProduct->quantity = $request->get("quantity");
            $updateProduct->deleted = $request->get("deleted");
            $updateProduct->product_description = $request->get("product_description");
            $updateProduct->image_name = $request->get("product_image");
            $updateProduct->save();

            $temp = array();
            array_push($temp, $updateProduct);
            $productInfo->productData = $temp;
            return response()->json($productInfo);
        }

        $productInfo->productData = array();
        return response()->json($productInfo);
    }

    function delete(Request $request)
    {
        $productInfo = new stdClass();

        $deleteProduct = Catalog::where('id', $request->get("productid"))->first();
        if ($deleteProduct != null) {
            $deleteProduct->deleted = 1;
            $deleteProduct->save();

            $temp = array();
            array_push($temp, $deleteProduct);
            $productInfo->productData = $temp;
            return response()->json($productInfo);
        }

        $productInfo->productData = array();
        return response()->json($productInfo);
    }

    function getProducts(Request $request)
    {
        $productInfo = new stdClass();

        if ($request->has("productid")) {
            $productInfo->productData = Catalog::where("id", $request->get("productid"))->get();
            return response()->json($productInfo);
        } else if ($request->has("producttype")) {
            $productInfo->productData = Catalog::where('product_type', $request->get("producttype"))->get();
            return response()->json($productInfo);
        } elseif ($request->has("productname")) {
            $productInfo->productData = Catalog::where('product_name', $request->get("productname"))->get();
            return response()->json($productInfo);
        }

        $productInfo->productData = array();
        return response()->json($productInfo);
    }

}
