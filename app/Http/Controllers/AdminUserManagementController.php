<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserRole;
use Illuminate\Http\Request;

class AdminUserManagementController extends Controller
{

    public function getAdminSearchPage()
    {
        return View("adminusermanagement");
    }

    public function getUsers(Request $request)
    {

        if ($request->has("userid") && strcmp($request->get("userid"), "") != 0) {
            $id = $request->get("userid");
            $userInformation = User::where('id', $id);
            if ($userInformation != null) {
                $userInformation = $userInformation->first();
                return response()->json($userInformation);
            }
            return "no match in id";
        } else if ($request->has("username") && strcmp($request->get("username"), "") != 0) {
            $username = $request->get("username");
            $userInformation = User::where('name', $username)->get();
            if ($userInformation != null) {
                $userInformation->objectsCount = count($userInformation);
                return response()->json($userInformation);
            }
            return "no match username";

        } else if ($request->has("useremail") && strcmp($request->get("useremail"), "") != 0) {
            $useremail = $request->get("useremail");
            $userInformation = User::where('email', $useremail);
            if ($userInformation != null) {
                $userInformation = $userInformation->first();
                return response()->json($userInformation);
            }
            return "no match in email";
        }
    }

    public function getUserRole(Request $request)
    {

        if ($request->has("userid")) {
            $getUserRole = UserRole::where('user_id', $request->get("userid"));
            if ($getUserRole != null) {
                $getUserRole = $getUserRole->first();
                return response()->json($getUserRole);
            }
            return "no user found";
        }

    }

    public function updateUserRole(Request $request)
    {

        if ($request->has("userid")) {
            $updateUserRole = UserRole::where('user_id', $request->get("userid"))->first();
            if ($updateUserRole != null) {
                $updateUserRole->role = $request->get("userrole");
                $updateUserRole->save();
                return response()-> json("role updated succesfully");
            } else {
                $newUserRole = new UserRole();
                $newUserRole->user_id = $request->get("userid");
                $newUserRole->role = $request->get("userrole");
                $newUserRole->user_name = User::where("id",$request->get("userid"))->first()->name;
                $newUserRole->save();
            }
        }
        return "cannot update user role";
    }
}
