<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CartInfoController extends Controller
{
    function getCartInfo(Request $request)
    {

        $itemCount = 0;
        if ($request->session()->has("cartInfo")) {
            $cartInfo = json_decode($request->session()->get("cartInfo"));
            foreach ($cartInfo as $cartItem) {
                $itemCount++;
            }
            return response()->json(["itemCount" => $itemCount, "cartInfo" => $cartInfo]);
        }

        return response()->json(["itemCount" => $itemCount]);

    }
}
