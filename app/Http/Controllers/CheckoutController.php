<?php

namespace App\Http\Controllers;

use App\Models\Address;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckoutController extends Controller
{
    protected static $SHIPPING_ADDRESS = "shipping";
    protected static $BILLING_ADDRESS = "billing";

    public function processCheckoutFlow()
    {
        $currentUserId = Auth::user()->id;
        $shippingAddress = $this->getCurrentShippingAddress();
        $billingAddress = $this->getCurrentBillingAddress();

        if ($shippingAddress != null && $billingAddress != null)
            return View('checkout', ["currentShipping" => $shippingAddress, "currentBilling" => $billingAddress]);
        return View('checkout');
    }

    public function saveAddress(Request $request): string
    {
        $newDefaultAddress = new Address();

        if ($request->has("newShippingAddress")) {

            /*make current default as non default*/
            $currentShippingAddress = $this->getCurrentShippingAddress();
            if ($currentShippingAddress != null) {
                $currentShippingAddress->current = 0;
                $currentShippingAddress->save();
            }
            $newDefaultAddress->address_type = CheckoutController::$SHIPPING_ADDRESS;

        } else if ($request->has("newBillingAddress")) {

            /*make current default as non default*/
            $currentBillingAddress = $this->getCurrentBillingAddress();
            if ($currentBillingAddress != null) {
                $currentBillingAddress->current = 0;
                $currentBillingAddress->save();
            }

            $newDefaultAddress->address_type = CheckoutController::$BILLING_ADDRESS;
        }


        /*get new value and save as default*/
        $newDefaultAddress->user_id = Auth::user()->id;
        $newDefaultAddress->name = $request->get("user_name");
        $newDefaultAddress->address_line_one = $request->get("adr_line_one");
        $newDefaultAddress->address_line_two = $request->get("adr_line_two");
        $newDefaultAddress->city = $request->get("city");
        $newDefaultAddress->state = $request->get("state");
        $newDefaultAddress->zipcode = $request->get("zipcode");
        $newDefaultAddress->current = 1;
        $newDefaultAddress->deleted = 0;

        $newDefaultAddress->save();

        return "Address Saved Successfully";

    }

    private function getCurrentShippingAddress()
    {
        $currentUserId = Auth::user()->id;
        return Address::where([
            ['user_id', $currentUserId],
            ['current', 1],
            ['address_type', CheckoutController::$SHIPPING_ADDRESS],
            ['deleted', 0]
        ])->first();
    }

    private function getCurrentBillingAddress()
    {
        $currentUserId = Auth::user()->id;
        return Address::where([
            ['user_id', $currentUserId],
            ['current', 1],
            ['address_type', CheckoutController::$BILLING_ADDRESS],
            ['deleted', 0]
        ])->first();
    }

    public function deleteAddress(Request $request) : string
    {
        if ($request->has("addressId")) {
            $addressToDelete = Address::where('id' , $request->get("addressId"))->first();
            $addressToDelete->deleted = 1;
            $addressToDelete->current = 0;
            $addressToDelete->save();

            return "Deleted Address of $addressToDelete->id";
        }


    }
}
