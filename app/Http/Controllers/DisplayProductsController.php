<?php

namespace App\Http\Controllers;

use App\Models\Catalog;
use App\Models\Prices;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class DisplayProductsController extends Controller
{
    private static $PAGINATION_SIZE = 8;

    function displayAllProducts(Request $request)
    {
        Paginator::useBootstrap();

        $products = $this->getProducts($request);

        $pricesArray = array();

        $itemCount = 0;
        if ($request->session()->has("cartInfo")) {
            $cartInfo = json_decode($request->session()->get("cartInfo"));
            foreach ($cartInfo as $cartItem) {
                $itemCount++;
            }
        }


        foreach ($products as $product) {
            $price = Prices::where('catalog_id', $product->id)->first();
            array_push($pricesArray, $price);
        }

        return View('dashboard', ['products' => $products, "prices" => $pricesArray,
            'itemCount' => $itemCount, "productCount" => count($products)]);
    }

    private function getProducts(Request $request)
    {
        if ($request->has("filter")) {
            if (strcmp($request->get("searchtype"), "category") == 0) {
                return Catalog::where([
                    ["product_type", $request->get("filter")],
                    ["deleted", 0]
                ])->paginate(DisplayProductsController::$PAGINATION_SIZE);
            } else {
                $products = Catalog::where([
                    ["product_type", $request->get("filter")],
                    ["deleted", 0]
                ])->paginate(DisplayProductsController::$PAGINATION_SIZE);

                if (count($products)) {
                    return $products;
                }
                return Catalog::where([
                    ["product_name", $request->get("filter")],
                    ["deleted", 0]
                ])->paginate(DisplayProductsController::$PAGINATION_SIZE);
            }
        }
        return Catalog::where('deleted', 0)->paginate(DisplayProductsController::$PAGINATION_SIZE);
    }
}
