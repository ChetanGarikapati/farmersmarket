<?php

namespace App\Http\Controllers;

use App\Models\OrderItems;
use App\Models\Orders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MyOrdersController extends Controller
{
    public function displayUserOrders()
    {
        $orders = Orders::where("user_id", Auth::user()->id)->get();
        if ($orders != null && count($orders) > 0) {
            return View("userorders", ["orders" => $orders]);
        } else {
            return View("userorders", ["noorders" => 1]);
        }
    }

    public function displayUserOrderDetails(Request $request)
    {
        if ($request->has("orderid")) {
            $order = Orders::where('id', $request->get("orderid"))->first();
            $orderItems = OrderItems::where('order_id', $request->get("orderid"))->get();

            return View("userorders", ["order" => $order, "orderItems" => $orderItems]);
        }

        return redirect("/myorders");
    }
}
