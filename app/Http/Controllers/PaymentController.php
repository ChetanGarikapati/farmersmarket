<?php

namespace App\Http\Controllers;

use App\Models\Catalog;
use App\Models\OrderItems;
use App\Models\Orders;
use App\Models\Prices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    private static $PENDING = "Pending";
    private static $COMPLETED = "Completed";
    private static $CANCELLED = "Cancelled";

    /**
     * @return string
     */
    public static function getPENDING(): string
    {
        return self::$PENDING;
    }

    /**
     * @return string
     */
    public static function getCOMPLETED(): string
    {
        return self::$COMPLETED;
    }

    /**
     * @return string
     */
    public static function getCANCELLED(): string
    {
        return self::$CANCELLED;
    }


    public function calculateOrderTotal(Request $request)
    {
        $cartInfo = json_decode($request->session()->get("cartInfo"));
        $existingOrder = Orders::where("order_status", PaymentController::$PENDING)->get();

        if (count($existingOrder) <= 0) {
            $orderItemsList = array();

            $totalQuantity = 0;
            $totalPrice = 0;
            foreach ($cartInfo as $cartItem => $quantity) {
                $totalQuantity += $quantity;

                $orderItem = new OrderItems();
                $priceInfo = Prices::where("product_name", $cartItem)->first();
                $orderItem->product_name = $cartItem;
                $orderItem->user_id = Auth::user()->id;
                $orderItem->product_id = $priceInfo->catalog_id;
                $orderItem->quantity = $quantity;
                $orderItem->order_item_status = PaymentController::$PENDING;
                if ($priceInfo->offer_price > 0) {
                    $orderItem->price = $priceInfo->offer_price * $quantity;
                } else {
                    $orderItem->price = $priceInfo->mrp * $quantity;
                }

                $totalPrice += $orderItem->price;
                array_push($orderItemsList, $orderItem);

            }


            $order = new Orders();
            $order->order_items_count = $totalQuantity;
            $order->user_id = Auth::user()->id;
            $order->order_total = $totalPrice;
            $order->order_status = PaymentController::$PENDING;
            $order->save();

            foreach ($orderItemsList as $orderItem) {
                $orderItem->order_id = $order->id;
                $orderItem->save();
            }

            return View('payment', ["cartQuantity" => $totalQuantity, "cartTotal" => $totalPrice,
                "orderItemsList" => $orderItemsList, "orderId" => $order->id]);
        } else {
            $existingOrder = $existingOrder->first();
            $orderItemsList = OrderItems::where("order_id", $existingOrder->id)->get();
            return View('payment', ["cartQuantity" => $existingOrder->order_items_count,
                "cartTotal" => $existingOrder->order_total,
                "orderItemsList" => $orderItemsList, "orderId" => $existingOrder->id]);
        }


    }


    public function placeOrder(Request $request)
    {
        $existingOrder = Orders::where([
            ["user_id", Auth::user()->id],
            ["order_status", PaymentController::$PENDING]
        ])->first();

        if ($existingOrder == null)
            return redirect("/");
        else {
            $existingOrderItems = OrderItems::where("order_id", $existingOrder->id)->get();
            foreach ($existingOrderItems as $orderItem) {
                $catalogItem = Catalog::where("id", $orderItem->product_id)->first();
                if ($catalogItem->quantity > $orderItem->quantity) {
                    $catalogItem->quantity -= $orderItem->quantity;
                    $catalogItem->save();

                    $orderItem->order_item_status = PaymentController::$COMPLETED;
                    $orderItem->save();
                } else {
                    $request->session()->forget("cartInfo");
                    return View('orderplaced', ["orderId" => $existingOrder->id, "failedOrderItems" => $orderItem]);
                }
            }
            $existingOrder->order_status = PaymentController::$COMPLETED;
            $existingOrder->save();
            $request->session()->forget("cartInfo");
            return View('orderplaced', ["orderId" => $existingOrder->id]);
        }
    }
}
