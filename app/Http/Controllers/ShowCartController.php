<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ShowCartController extends Controller
{
    function displayCart(Request $request)
    {


        $cartInfo = json_decode($request->session()->get("cartInfo"));
        return View('cart', ["cartInfo" => $cartInfo]);
    }
}
