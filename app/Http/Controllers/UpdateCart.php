<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UpdateCart extends Controller
{
    function saveCartInfo(Request $request)
    {
        $request->session()->put("cartInfo", $request->get("cartInfo"));
    }
}
