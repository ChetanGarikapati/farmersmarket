<?php

namespace App\Http\Controllers;


use App\Actions\Fortify\UpdateUserPassword;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserProfileController extends Controller
{
    public function displayUserProfile()
    {
        $user = User::where('id', Auth::user()->id)->first();
        return View("profile", ["user" => $user]);
    }

    public function updateUserPassword(Request $request)
    {
        if ($request->has("current_password") && $request->has("password")) {
            $input = array();
            $input["current_password"] = $request->get("current_password");
            $input["password"] = $request->get("password");
            $input["password_confirmation"] = $request->get("password_confirmation");

            $updateUserPassword = new UpdateUserPassword();
            $response = $updateUserPassword->update(Auth::user(), $input);

            if ($response == null) {
                return response("password change successfully");
            }
            return response()->json($response);
        }
    }
}
