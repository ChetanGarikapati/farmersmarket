<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string("name")->nullable(false);
            $table->string("address_type")->nullable(false);;
            $table->string("address_line_one")->nullable(false);;
            $table->string("address_line_two");
            $table->string("city")->nullable(false);;
            $table->string("state")->nullable(false);;
            $table->string("zipcode")->nullable(false);;
            $table->boolean("current");
            $table->boolean("deleted")->default(0);
            $table->unsignedBigInteger("user_id");
            $table->foreign("user_id")->references("id")->on("users");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
