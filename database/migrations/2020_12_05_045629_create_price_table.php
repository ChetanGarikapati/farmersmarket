<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->double("mrp")->nullable(false);
            $table->double("offer_price")->default(0.0);
            $table->string("product_name")->unique();
            $table->unsignedBigInteger("catalog_id");
            $table->foreign("catalog_id")->references("id")->on("catalog");
            $table->boolean("deleted")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices');
    }
}
