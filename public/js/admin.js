$(document).ready(function () {

    $(".operation-item").mouseover(function () {
        $(this).addClass("shadow");
        $(this).addClass("bg-light");
    });

    $(".operation-item").mouseleave(function () {
        $(this).removeClass("shadow");
        $(this).removeClass("bg-light");
    });
});
