$(document).ready(function () {

    if ($("#current_shipping").length) {
        $("#shipping-address-form-holder").hide();
    }

    if ($("#current_billing").length) {
        $("#billing-address-form-holder").hide();
    }

    $("#new_billing_success").hide();
    $("#new_shipping_success").hide();
    /**
     * save shipping and/or billing address
     */
    $("#shipping-address-form").submit(function (event) {
        event.preventDefault();
        $("#new_billing_success").hide();
        $("#new_shipping_success").hide();

        $.ajax({
            url: "/checkout/saveAddress",
            method: "post",
            data: $("#shipping-address-form").serialize() + "&newShippingAddress=true",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                console.log(response);
                $("#new_shipping_success").show();
                location.href = "/checkout";
            }
        });

        if ($("#same_as_shipping").is(":checked")) {

            $("#billing-address-form-holder").hide();
            $("#current_billing").hide();

            $.ajax({
                url: "/checkout/saveAddress",
                method: "post",
                data: $("#shipping-address-form").serialize() + "&newBillingAddress=true",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    console.log(response);
                    $("#new_billing_success").show();
                    location.href = "/checkout";
                }
            });
        }
    });

    $("#billing-address-form").submit(function (event) {
        event.preventDefault();
        $("#new_billing_success").hide();
        $("#billing-address-form").serialize();
        $.ajax({
            url: "/checkout/saveAddress",
            method: "post",
            data: $("#billing-address-form").serialize() + +"&newBillingAddress=true",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                console.log(response)
                $("#new_billing_success").show();
            }
        });
    });

});

function showNewShippingAddress() {
    $("#shipping-address-form-holder").show();
    $("#current_shipping").hide();
}

function showNewBillingAddress() {
    $("#billing-address-form-holder").show();
    $("#current_billing").hide();
}

function deleteAddress(addressId) {
    $.ajax({
        url: "/checkout/deleteAddress",
        method: "delete",
        data : {"addressId": addressId},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            location.href = "/checkout";
        }
    });
}
