$(document).ready(function () {

    $(".catalog-button").mouseover(function () {
        $(this).addClass("shadow");
    });

    $(".catalog-button").mouseleave(function () {
        $(this).removeClass("shadow");
    });

    $("#fruits-button").click(function () {
        location.href = "/dashboard?filter=fruit&searchtype=category";
    });

    $("#vegetables-button").click(function () {
        location.href = "/dashboard?filter=vegetable&searchtype=category";
    });

    $("#product-search-button").click(function () {
        if ($("#product-search-box").val().length > 0) {
            location.href = "/dashboard?filter=" + $("#product-search-box").val() + "&searchtype=input";
        }
    });

});
