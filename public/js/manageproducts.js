$(document).ready(function () {
    $("#no-results-block").hide();
    $("#update-check").hide();
    $("#multiple-results-list-holder").hide();


    $("#results-form").on("submit", function (event) {
        event.preventDefault();
        updateProduct();
    })
});

function searchProducts() {

    let url;
    $("#no-results-block").hide();
    $("#update-check").hide();
    $("#updateResult").hide();
    $("#multiple-results-list-holder").hide();

    if ($("#productid-search").val().length > 0) {
        url = "/admin/manageproducts/getproduct?productid=" + $("#productid-search").val();
    } else if ($("#producttype-search").val().length > 0) {
        url = "/admin/manageproducts/getproduct?producttype=" + $("#producttype-search").val();
    } else if ($("#productname-search").val().length > 0) {
        url = "/admin/manageproducts/getproduct?productname=" + $("#productname-search").val()
    }

    performSearch(url);
}

function updateProduct() {
    $("#updateResult").hide();
    $("#update-check").hide();
    $("#multiple-results-list-holder").hide();

    if ($("#productid").val().length <= 0) {
        $("#update-check").show();
        return;
    }

    $.ajax({
        url: "/admin/manageproducts/updateproduct?productid=" + $("#productid").val(),
        method: "post",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: $("#results-form").serialize(),
        success: function (response) {
            console.log(response);
            if (response.productData.length == 1) {
                displaySingleProduct(response.productData)
                $("#updateResult").text("products updates successfully");
                $("#updateResult").show();
            }
        }
    })
}

function insertProduct() {
    $("#update-check").hide();
    $("#multiple-results-list-holder").hide();

    $.ajax({
        url: "/admin/manageproducts/saveproduct",
        method: "post",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: $("#results-form").serialize(),
        success: function (response) {
            console.log(response);
            if (response.productData.length == 1) {
                displaySingleProduct(response.productData)
                $("#updateResult").text("products saved successfully");
            }
        }
    })
}

function deleteProduct() {
    $("#updateResult").hide();
    $("#update-check").hide();
    $("#multiple-results-list-holder").hide();

    if ($("#productid").val().length <= 0) {
        $("#update-check").show();
        return;
    }

    $.ajax({
        url: "/admin/manageproducts/deleteproduct?productid=" + $("#productid").val(),
        method: "delete",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            console.log(response);
            if (response.productData.length == 1) {
                displaySingleProduct(response.productData)
                $("#updateResult").text("products deleted successfully");
                $("#updateResult").show();
            }
        }
    })
}

function displaySingleProduct(response) {
    $("#multiple-results-list-holder").hide();

    $.each(response, function (key, response) {
        console.log(response)
        $("#productid").val(response.id);
        $("#product_name").val(response.product_name);
        $("#product_type").val(response.product_type);
        $("#product_description").val(response.product_description);
        $("#quantity").val(response.quantity);
        $("#deleted").val(response.deleted);
        $("#product_image").val(response.image_name);
    })

}

function displayMultipleResults(response) {
    $("#multiple-results-list-holder").show();
    $("#multiple-results-list").text("");
    $.each(response, function (key, value) {
        $("#multiple-results-list")
            .append("<button onclick='getSelectedProduct(" + value.id + ")' class='order-list list-group-item'> ProductId : " + value.id + ",Product Name : " + value.product_name + "</button>")
    })
}

function getSelectedProduct(id) {
    let url;
    $("#no-results-block").hide();
    $("#update-check").hide();
    $("#updateResult").hide();


    url = "/admin/manageproducts/getproduct?productid=" + id;

    performSearch(url);
}

function performSearch(url) {
    if (url.length > 0) {
        $.ajax({
            url: url,
            method: "get",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                console.log(response);
                if (response.productData.length <= 0) {
                    $("#no-results-block").show();
                } else if (response.productData.length == 1) {
                    displaySingleProduct(response.productData);
                } else if (response.productData.length > 1) {
                    displayMultipleResults(response.productData);
                }
            }

        });
    }
}
