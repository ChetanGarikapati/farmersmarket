$(document).ready(function () {
    $("#search_user_form").on("submit", function (event) {
        event.preventDefault();
    });

    $("#message_notifications").hide();

    $('input:checkbox').change(function () {
        if ($(this).is(":checked")) {
            $('#edit_form_userrole').attr("disabled", "disabled");
        } else {
            $('#edit_form_userrole').removeAttr("disabled");
        }
    });
});

function getUsers() {
    var defaultMessage = document.getElementById("success_message");
    defaultMessage.innerText = "";
    document.user_info_form.reset();
    $("#display-data-field-for-single-user").hide();
    $.ajax({
        url: "/admin/getusers",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "get",
        data: $("#search_user_form").serialize(),
        success: function (response) {
            var flag = false;
            let users = [];
            $.each(response, function (key, value) {
                if (typeof (key) === "number") {
                    flag = true;
                    let user = new Object();
                    $.each(value, function (key, value) {
                        user[key] = value;
                    });
                    users.push(user);
                }
            });

            if (flag == true) {
                multipleUsers(users);
            } else {
                singleUser(response);
                getUserRole(response.id);
            }
        }
    });
}

function multipleUsers(users) {
    $("#display-data-field-for-single-user").hide();
    $("#userslist").text("");
    for (var i = 0; i < users.length; i++) {
        $("#userslist").append('<li class="shadow p-2 m-2"  id=\"' + users[i].id + '"><button type="button" class="btn btn-lg btn-link" style ="text-decoration: none;" onclick="fetchUserDetails(\'' + users[i].id + '\');">user-id : ' + users[i].id + '&emsp; user-name : ' + users[i].name + '&emsp; user-email : ' + users[i].email + '</button></li>');
    }
}

function singleUser(response) {
    $("#userslist").text("");

    $("#edit_form_userid").val(response.id);
    $("#edit_form_useremail").val(response.email);
    $("#edit_form_username").val(response.name);
    $("#display-data-field-for-single-user").show();
}

function fetchUserDetails(id) {
    $.ajax({
        url: "/admin/getusers?userid=" + id,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "get",
        success: function (response) {
            getUserRole(id);
            singleUser(response);
            $("#display-data-field-for-single-user").show();
        }
    });
}

function getUserRole(id) {
    $.ajax({
        url: "/admin/getuserrole?userid=" + id,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "get",
        success: function (response) {
            $("#edit_form_userrole").val(response.role);
        }
    });
}


function makeAsAdmin() {
    if ($("#to_make_admin").prop("checked")) {
        $.ajax({
            url: "/admin/updateuserrole?userid=" + $("#edit_form_userid").val() + "&userrole=admin",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: "post",
            success: function (response) {
                var success_message = document.getElementById("success_message");
                success_message.innerHTML = response;
                $("#edit_form_userrole").val("admin");
                $("#message_notifications").show()
            }
        });
    } else {
        $.ajax({

            url: "/admin/updateuserrole?userid=" + $("#edit_form_userid").val() + "&userrole=" + $("#edit_form_userrole").val(),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: "post",
            success: function (response) {
                var success_message = document.getElementById("success_message");
                success_message.innerHTML = response;

                var role = $("#edit_form_userrole").val();
                $("#edit_form_userrole").val(role);

                $("#message_notifications").show()
            }
        });
    }
}
