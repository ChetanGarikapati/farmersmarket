$(document).ready(function () {
    $("#results-field").hide();
    $("#no-results-block").hide();
    $("#multiple-results-field").hide();
});

function searchOrders() {
    let url;

    $("#results-field").hide();
    $("#no-results-block").hide();
    $("#multiple-results-field").hide();
    $("#order-items-list").text("");
    $("#updateResult").text("");
    $("#multiple-results-list").text("");

    if ($("#orderid-search").val().length > 0) {
        url = "/admin/manageorders" + "?orderid=" + $("#orderid-search").val();
    } else if ($("#userid-search").val().length > 0) {
        url = "/admin/manageorders" + "?userid=" + $("#userid-search").val()
    } else return;

    $.ajax({
        url: url,
        method: "get",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            console.log(response);
            if (response.orderData.length <= 0) {
                $("#no-results-block").show();
            } else if (response.orderData.length == 1) {
                displaySingleOrderInfo(response);
            } else {
                displayMiltipleResults(response);
            }
        }
    });
}

function displayMiltipleResults(response) {
    $("#multiple-results-list").text("");

    $.each(response, function (key, value) {
        $.each(value, function (key, value) {
            console.log(value.id);
            $("#multiple-results-list").append("<button onclick='getSelectedOrder(" + value.id + ")' class='order-list list-group-item'> OrderId : " + value.id + ",Order Total : $" + value.order_total +
                ",Order Status : " + value.order_status + "</button>")
        })
    });

    $("#multiple-results-field").show();
}

function displaySingleOrderInfo(response) {
    $("#order-orderid").val(response.orderData[0].id);
    $("#order-user-id").val(response.orderData[0].user_id);
    $("#order-item-count").val(response.orderData[0].order_items_count);
    $("#order-status").val(response.orderData[0].order_status);
    $("#order-total").val(response.orderData[0].order_total);

    $.each(response.orderItemData, function (key, value) {

        $("#order-items-list").append("<li>Product : " + value.product_name + " , Quantity : " + value.quantity + "</li>");
    })

    $("#results-field").show();
}

function updateOrderStatus() {

    $("#updateResult").text("");

    let url = "/admin/updateorder?orderid=" + $("#order-orderid").val() + "&updated-status=" + $("#order-status").val();
    $.ajax({
        url: url,
        method: "get",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {

            $("#updateResult").text(response)
        }
    });
}

function getSelectedOrder(id) {
    $("#multiple-results-field").hide();

    let url = "/admin/manageorders" + "?orderid=" + id;
    $.ajax({
        url: url,
        method: "get",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if (response.orderData.length <= 0) {
                $("#no-results-block").show();
            } else if (response.orderData.length == 1) {
                displaySingleOrderInfo(response);
            } else {
                displayMiltipleResults(response);
            }
        }
    });
}


