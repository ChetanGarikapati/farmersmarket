$(document).ready(function () {
    $(".card").mouseover(function () {
        $(this).addClass("shadow");
    });

    $(".card").mouseleave(function () {
        $(this).removeClass("shadow");
    });

    $.ajax({
        url: "/cartInfo",
        success: function (data) {
            $("#cart-info").text(data.itemCount);
            $.each(data.cartInfo, function (key, value) {
                items.set(key, value);
            });
            if (window.location.pathname == "/showCart") {
                removeFromCart("");
            }
        }
    });

});

function stepUp(productReference) {
    let increment = parseInt($("#" + productReference).text()) + 1;
    $("#" + productReference).text(increment);
}

function stepDown(productReference) {
    let decrement = parseInt($("#" + productReference).text()) - 1;
    if (decrement >= 0) {
        $("#" + productReference).text(decrement);
    }
}

//map to hold the items of cart
const items = new Map();

function updateCart(item) {
    if (parseInt($("#" + item + "-count").text()) == 0 && items.has(item)) {
        items.delete(item);
    } else if (parseInt($("#" + item + "-count").text()) != 0) {
        if (items.has(item)) {
            items.set(item, parseInt($("#" + item + "-count").text()) + parseInt(items.get(item)));
        } else {
            items.set(item, parseInt($("#" + item + "-count").text()));
        }
    }

    $("#cart-info").text(items.size); //updates cart info in navbar
    if (items.size > 0) {
        const cart = Object.fromEntries(items);

        $.ajax({
            url: "/updateCart",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: "post",
            data: {'cartInfo': JSON.stringify(cart)}
        });
    }
}

function removeFromCart(item) {
    if (items.has(item)) {
        items.delete(item);
        $("#cart-info").text(items.size); //updates cart info in navbar
        const cart = Object.fromEntries(items);
        $.ajax({
            url: "/updateCart",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: "post",
            data: {'cartInfo': JSON.stringify(cart)}
        });
        $("#" + item).css('display', 'none');
    }

    if (items.size == 0) {
        $("#return-to-shopping").css('display', 'block');
        $("#checkout").css('display', 'none');
    } else {
        $("#return-to-shopping").css('display', 'none');
        $("#checkout").css('display', 'block');
    }

}

class Cart {
}

function showCart() {
    var redirect = '/showCart';
    var form = '';

    form += '<input type="hidden" name="_token" value="' + $('meta[name="csrf-token"]').attr('content') + '">';
    $('<form action="' + redirect + '" method="GET">' + form + '</form>').appendTo($(document.body)).submit();

}







