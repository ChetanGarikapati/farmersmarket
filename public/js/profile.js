function changePassword() {
    console.log($("#change-password-form").serialize());
    $("#no-match-text").hide();
    $("#no-match-text").hide();
    $("#result-success").hide();

    if ($("#password_confirmation").val() !== $("#password").val()) {
        $("#no-match-text").show();
    }

    $.ajax({
        url: "/profile/updatepassword",
        method: "post",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: $("#change-password-form").serialize(),
        success: function (response) {
            console.log(response)
            if (response === "password change successfully") {
                $("#result-success").text(response);
                $("#result-success").show();
                $("#result-fail").hide();
            }
        },
        error: function (response) {
            $("#result-fail").text("Password change failed");
            $("#result-fail").show();
            $("#result-success").hide();
        }
    });
}

$(document).ready(function () {

    $("#no-match-text").hide();
    $("#result-success").hide();
    $("#result-fail").hide();

    $("#change-password-form").on("submit", function (event) {
        event.preventDefault();
        changePassword();
    })
});
