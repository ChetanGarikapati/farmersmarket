@extends('welcome')

@section('content')
    <div id="admin-heading" class="d-flex justify-content-center">
        <h1 class="shadow p-3">Admin Console</h1>
    </div>

    <br>

    <ul id="amin-operations">
        <li id="user-management" class="p-3">
            <div class="row">
                <p class="col-sm-3 text-justify text-wrap">
                    Make changes to user accounts and manage privileges of the registered
                    user and also create new administrators.
                </p>
                <h3 class="col-sm-3 p-3 m-2 border border-primary text-center operation-item rounded">
                    <a href="/admin/manageuser/">Manage Users</a>
                </h3>
            </div>
        </li>
        <br/>
        <li id="product-management" class="p-3">
            <div class="row">
                <p class="col-sm-3 text-wrap text-justify">
                    Manage the products in store catalog like adding, deleting, modifying and also manage the inventory
                    of products.
                </p>
                <h3 class="col-sm-3 m-2 p-3 border border-dark text-center operation-item rounded">
                    <a href="/admin/manageproducts">Manage Products</a>
                </h3>
            </div>
        </li>
        <br/>
        <li id="order-management" class="p-3">
            <div class="row">
                <p class="col-sm-3 text-justify text-wrap">
                    Mange the existing orders created by the users, modify and cancel orders and also create new orders
                    for any user.
                </p>
                <h3 class="col-sm-3 m-2 p-3 border border-success text-center operation-item rounded">
                    <a href="/admin/manageorders/">Manage Orders</a>
                </h3>
            </div>
        </li>
    </ul>

    <script src="/js/admin.js" defer></script>
@endsection

