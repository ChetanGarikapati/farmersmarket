@extends("welcome")

@section("content")
    <div>
        <div id="search-form" class="shadow p-3 m-3 border border-dark">
            <form id="search_user_form" name ="search_user_form">
                <div class="row m-3">
                    <div class="form-group input-group col-sm-4">
                        <div class="input-group-prepend">
                            <span class="shadow input-group-text">Search by Id</span>
                        </div>
                        <input class="shadow form-control" id="userid" name="userid" placeholder="Enter User Id.."
                               type="number">
                    </div>
                    <div class="form-group input-group  col-sm-4">
                        <div class="input-group-prepend">
                            <span class="shadow input-group-text">Search by Email</span>
                        </div>
                        <input class="form-control shadow" id="useremail" name="useremail"
                               placeholder="Enter User Email.." type="email">
                    </div>
                    <div class="form-group input-group col-sm-4">
                        <div class="input-group-prepend">
                            <span class="shadow input-group-text">Search by UserName</span>
                        </div>
                        <input class="shadow form-control" id="username" name="username" placeholder="Enter User Name.."
                               type="text">
                    </div>
                </div>
                <div class="p-3 m-3">
                    <button class="btn btn-lg btn-primary shadow" onclick="getUsers()">Search</button>
                    <button class="btn btn-lg btn-danger shadow" type="reset">Reset</button>
                </div>
            </form>
        </div>

        <br>

        <div id="display-data_field-for-multiple-users" class="col-sm-8 p-3 ">
            <ul class="list-unstyled" id="userslist">
            </ul>
        </div>
        <div id="display-data-field-for-single-user" class="shadow p-3 m-3 border border-dark" style="display: none">
            <form id="user_info_form" name="user_info_form">
                <div class="row m-3">
                    <div class="form-group input-group col-sm-4">
                        <div class="input-group-prepend">
                            <span class="shadow input-group-text">User Id</span>
                        </div>
                        <input class="shadow form-control" id="edit_form_userid" name="userid" type="number" disabled  />
                    </div>
                </div>
                <div class="row m-3">
                    <div class="form-group input-group  col-sm-4">
                        <div class="input-group-prepend">
                            <span class="shadow input-group-text">User Name</span>
                        </div>
                        <input class="form-control shadow" id="edit_form_username" name="username"  disabled />
                    </div>
                </div>
                <div class="row m-3">
                    <div class="form-group input-group col-sm-4">
                        <div class="input-group-prepend">
                            <span class="shadow input-group-text">Email</span>
                        </div>
                        <input class="shadow form-control" id="edit_form_useremail" name="useremail"  disabled />
                    </div>
                </div>
                <div class="row m-3">
                    <div class="form-group input-group col-sm-4">
                        <div class="input-group-prepend">
                            <span class="shadow input-group-text">Role</span>
                        </div>
                        <input class="shadow form-control" id="edit_form_userrole" name="userrole"  />
                    </div>
                </div>
                <div class="row p-3 m-3">
                    <label ><input type="checkbox" name="to_make_admin" id="to_make_admin" style="height: 20px;width: 20px">&nbsp;<b style="font-size: x-large">&nbsp;Upgrade to Admin</b> &nbsp;</label>
                </div>
                <div class="row p-3 m-3">
                    <button type ="button" class="btn btn-lg btn-warning shadow" onclick="makeAsAdmin()" >Update</button>
                    &nbsp;&nbsp;
                    <span id="message_notifications" >
                        <div id="success_message" style="color: #00ff00; text-indent:1em; font-size: x-large"></div>
                    </span>
                </div>
            </form>
        </div>
    </div>

    <script src="/js/manageuser.js" defer></script>
@endsection()
