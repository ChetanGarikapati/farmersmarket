@extends('welcome')

@section('content')
    <div class="d-flex justify-content-center">
        <h1 class="shadow col-sm-6 p-3 text-center">Your Shopping Cart</h1>
    </div>
    <br> <br>
    <div id="cart-items">
        @if($cartInfo != null)
            <ul class="list-group col-sm-4">
                @foreach($cartInfo as $cartItem => $quantity)
                    <li class="list-group-item list-group-item-action shadow p-3 m-3" id="{{$cartItem}}">
                        <img class="img-thumbnail float-left" width="100" height="100" src="images/{{$cartItem}}.jpeg">
                        <h5 class="d-inline-flex mt-3 m-2 p-2 font-weight-bold">
                            {{ ucfirst($cartItem)}} &nbsp; &nbsp;
                            <span>Qty : {{$quantity}}</span>
                        </h5>
                        <button class="btn btn-danger float-right shadow m-3" onclick="removeFromCart('{{$cartItem}}')">
                            Remove
                            Item
                        </button>
                    </li>
                @endforeach
            </ul>
            <br>
            <div id="checkout" class="m-3">
                <a href="/checkout" class="btn shadow btn-lg btn-primary">Checkout</a>
                <a href="/" class="btn shadow btn-lg btn-success">Continue Shopping</a>
            </div>
        @endif
    </div>
    <div class="shadow p-3" id="return-to-shopping">
        <div class="justify-content-center text-center">
            <h3>Your cart is empty return to shopping !!</h3>
            <br/>
            <br/>
            <a href="/" class="btn shadow btn-lg btn-success">Go Back To Shopping</a>
        </div>
    </div>
@endsection
