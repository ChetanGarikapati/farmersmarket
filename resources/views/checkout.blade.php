@extends("welcome")

@section("content")
    <div id="address-header" class="d-flex justify-content-center">
        <h1 class="shadow col-sm-6 p-3 text-center">Addresses</h1>
    </div>
    <br> <br>

    @isset($currentShipping)
        @if($currentShipping != null)
            <div id="current_shipping">
                <div id="current_shipping_info" class="shadow border border-dark p-3 m-3 col-sm-6">
                    <h2>Current Shipping Address</h2>
                    <div class="text-left">
                        {{$currentShipping->name}} <br>
                        {{$currentShipping->address_line_one}} <br>
                        {{$currentShipping->address_line_two}} <br>
                        {{$currentShipping->city}} , {{$currentShipping->state}} <br>
                        {{$currentShipping->zipcode}}
                    </div>
                    <br>
                    <button class="btn btn-primary shadow" onclick="showNewShippingAddress()">Add New Shipping Address
                    </button>
                    <button class="btn btn-danger shadow" onclick="deleteAddress({{$currentShipping->id}})">Delete Address</button>
                </div>
            </div>
        @endif
    @endisset

    <br>

    @isset($currentBilling)
        @if($currentBilling != null)
            <div id="current_billing">
                <div id="current_billing_info" class="shadow border border-dark p-3 m-3 col-sm-6">
                    <h2>Current Billing Address</h2>
                    <div class="text-left">
                        {{$currentBilling->name}} <br>
                        {{$currentBilling->address_line_one}} <br>
                        {{$currentBilling->address_line_two}} <br>
                        {{$currentBilling->city}} , {{$currentBilling->state}} <br>
                        {{$currentBilling->zipcode}}
                    </div>
                    <br>
                    <button class="btn btn-primary shadow" onclick="showNewBillingAddress()">Add New Billing Address
                    </button>
                    <button class="btn btn-danger shadow" onclick="deleteAddress({{$currentShipping->id}})">Delete Address</button>
                </div>
            </div>
        @endif
    @endisset

    <br>

    <div id="shipping-address-form-holder">
        <div class="shadow border border-dark p-3 m-3 col-sm-6">
            <h3 class="col-sm-6 p-3 m-3">Shipping Address</h3>
            <form id="shipping-address-form" class="p-3">
                <div class="form-group input-group shadow">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Name</span>
                    </div>
                    <input type="text" class="form-control" required name="user_name" placeholder="Name">
                </div>
                <div class="form-group input-group shadow">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Address Line 1</span>
                    </div>
                    <input type="text" class="form-control" required name="adr_line_one" placeholder="StreetName">
                </div>
                <div class="form-group input-group shadow">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Address Line 2</span>
                    </div>
                    <input type="text" class="form-control" name="adr_line_two" placeholder="Apartment, Suite">
                </div>
                <div class="form-group input-group shadow">
                    <div class="input-group-prepend">
                        <span class="input-group-text">City</span>
                    </div>
                    <input type="text" class="form-control" required name="city" placeholder="Enter city name - Dallas">
                </div>
                <div class="form-group input-group shadow">
                    <div class="input-group-prepend">
                        <span class="input-group-text">State</span>
                    </div>
                    <input type="text" class="form-control" required name="state" placeholder="Enter full name - Texas">
                </div>
                <div class="form-group input-group shadow">
                    <div class="input-group-prepend ">
                        <span class="input-group-text">Zip</span>
                    </div>
                    <input type="number" class="form-control" required name="zipcode" placeholder="12345">
                </div>
                <br>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" name="same_as_shipping" id="same_as_shipping"
                           class="custom-control-input shadow">
                    <label class="custom-control-label" for="same_as_shipping">Billing Address Same as Shipping</label>
                </div>
                <br>
                <button type="submit" class="btn btn-success shadow">Save</button>
                <button type="reset" class="btn btn-danger shadow">Clear</button>
            </form>
            <p id="new_shipping_success" class="text-success">
                New Address Saved
            </p>
        </div>

    </div>

    <br>

    <div id="billing-address-form-holder">
        <div class="shadow border border-dark p-3 m-3 col-sm-6">
            <h3 class="col-sm-6 p-3 m-3">Billing Address</h3>
            <form id="billing-address-form" class="p-3">
                <div class="form-group input-group shadow">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Name</span>
                    </div>
                    <input type="text" class="form-control" required name="user_name" placeholder="Name">
                </div>
                <div class="form-group input-group shadow">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Address Line 1</span>
                    </div>
                    <input type="text" class="form-control" required name="adr_line_one"
                           placeholder="StreetName">
                </div>
                <div class="form-group input-group shadow">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Address Line 2</span>
                    </div>
                    <input type="text" class="form-control" name="adr_line_two" placeholder="Apartment, Suite">
                </div>
                <div class="form-group input-group shadow">
                    <div class="input-group-prepend">
                        <span class="input-group-text">City</span>
                    </div>
                    <input type="text" class="form-control" required name="city"
                           placeholder="Enter city name - Dallas">
                </div>
                <div class="form-group input-group shadow">
                    <div class="input-group-prepend">
                        <span class="input-group-text">State</span>
                    </div>
                    <input type="text" class="form-control" required name="state"
                           placeholder="Enter full name - Texas">
                </div>
                <div class="form-group input-group shadow">
                    <div class="input-group-prepend ">
                        <span class="input-group-text">Zip</span>
                    </div>
                    <input type="text" class="form-control" required name="zipcode" placeholder="12345">
                </div>
                <br>
                <button type="submit" class="btn btn-success shadow">Save</button>
                <button type="reset" class="btn btn-danger shadow">Clear</button>
            </form>
            <p id="new_billing_success" class="text-success">
                New Billing Address Saved
            </p>
        </div>
    </div>

    <div id="continue_payment_section" class="m-3">
        <a class="btn btn-primary shadow" href="/payment">Continue To Payment</a>
    </div>

    <script src="/js/checkout.js" defer></script>
@endsection
