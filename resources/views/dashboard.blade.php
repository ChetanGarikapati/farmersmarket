@extends('welcome')


@section('content')
    <div class="row">
        @for($i=0; $i<count($products); $i++)
            <div class="col-sm-3 mb-3">
                <div class="card">
                    <img src="/images/{{$products[$i]->image_name}}" alt="{{$products[$i]->product_name}}"
                         class="img-thumbnail card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">{{$products[$i]->product_name}}</h5>
                        <p class="card-text">
                            {{$products[$i]->product_description}}
                            @if($products[$i]->quantity <= 0) <span class="m-2 text-danger">OUT OF STOCK</span> @endif
                        </p>
                        <div class="d-inline-flex">
                            <button class="col btn btn-primary" onclick="updateCart('{{$products[$i]->product_name}}')"
                                    @if($products[$i]->quantity <= 0) disabled @endif>
                                Add
                                to Cart
                            </button>
                            <div class="m-1">
                                Price : ${{$prices[$i]->mrp}}
                            </div>
                            <div class="btn-group col float-right" role="group">
                                <button type="button" class="btn btn-secondary"
                                        onclick="stepDown('{{$products[$i]->product_name}}-count')">-
                                </button>
                                <button type="text" class="" name="{{$products[$i]->product_name}}-count"
                                        id="{{$products[$i]->product_name}}-count" value="1">1
                                </button>
                                <button type="button" class=" btn btn-success"
                                        onclick="stepUp('{{$products[$i]->product_name}}-count')">+
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        @endfor
    </div>
    <div class="d-flex justify-content-center">
        <span class="page-item">
          <a class="page-link" href="{{$products->withQueryString()->url(1)}}"> First Page  </a>
        </span>
        {{$products->withQueryString()->links()}} <br>
        <span class="page-item">
          <a class="page-link" href="{{$products->withQueryString()->url($products->lastPage())}}"> Last Page  </a>
        </span>
    </div>
@endsection
