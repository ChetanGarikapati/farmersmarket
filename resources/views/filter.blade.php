<div class="m-3 " id="filter-holder">
    <div id="filter-bar" class="m-3 p-3">
        <h3 class="d-inline m-3">Catalog</h3>
        <button id="fruits-button" class="catalog-button btn btn-warning ">Fruits</button>
        <button id="vegetables-button" class="catalog-button btn btn-primary btn-success ">Vegetables</button>
        <input id="product-search-box" class="form-control d-inline col-sm-6 m-2" type="text"
               placeholder="Search Products">
        <button id="product-search-button" class="btn btn-primary">Search Product</button>
    </div>

    <script src="/js/filter.js" defer></script>
</div>
