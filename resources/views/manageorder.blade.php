@extends('welcome')

@section('content')
    <div id="admin-heading" class="d-flex justify-content-center">
        <h1 class="shadow p-3">Admin Console</h1>
    </div>

    <br>

    <div id="order-search-holder" class="shadow border border-dark m-3 p-3">
        <div class="justify-content-center d-flex">
            <h3>Search Orders</h3>
        </div>
        <div class="row p-3 d-flex justify-content-center">
            <input id="orderid-search" type="number" class="col-sm-4 m-2 form-control" placeholder="Search by OrderId">
            <input id="userid-search" type="number" class="col-sm-4 m-2 form-control" placeholder="Serach by UserId">
            <button onclick="searchOrders()" class="col-sm-2 m-2 btn btn-primary form-control">Find Orders</button>
        </div>
    </div>

    <div id="results-field" class="shadow m-3 p-3">
        <h4>Order Details</h4>
        <div id="results-form">
            <div class="input-group form-group">
                <div class="input-group-prepend"><span class="input-group-text">Order Id</span></div>
                <input id="order-orderid" type="text" disabled class="form-control">
            </div>
            <div class="input-group form-group">
                <div class="input-group-prepend"><span class="input-group-text">User Id</span></div>
                <input id="order-user-id" type="text" disabled class="form-control">
            </div>
            <div class="input-group form-group">
                <div class="input-group-prepend"><span class="input-group-text">Items Count</span></div>
                <input id="order-item-count" type="text" disabled class="form-control">
            </div>
            <div class="input-group form-group">
                <div class="input-group-prepend"><span class="input-group-text">Order Status</span></div>
                <input id="order-status" type="text" class="form-control" data-toggle="tooltip"
                       title="Valid Status P-Pending, C-Completed, X-Cancelled, R-Return/Refund">
                </span>
            </div>
            <div class="input-group form-group">
                <div class="input-group-prepend"><span class="input-group-text">Order Total</span></div>
                <input id="order-total" type="text" disabled class="form-control">
            </div>
            <div>
                <h6>Order Items List</h6>
                <ul id="order-items-list">
                </ul>
            </div>
            <button class="btn btn-warning" onclick="updateOrderStatus()">Update Order Status</button>
            <br>
            <p id="updateResult" class="text-success"></p>
        </div>
    </div>

    <div id="multiple-results-field" class="shadow m-3 p-3">
        <h4>Order Details</h4>
        <ul id="multiple-results-list" class="list-group">
            
        </ul>
    </div>

    <div id="no-results-block" class="m-3 p-3 shadow justify-content-center">
        <h4>No Records Were Found</h4>
    </div>

    <script src="/js/ordermanagement.js" defer></script>
@endsection
