@extends('welcome')

@section('content')
    <div id="admin-heading" class="d-flex justify-content-center">
        <h1 class="shadow p-3">Admin Console</h1>
    </div>

    <br>

    <div id="product-search-holder" class="shadow border border-dark m-3 p-3">
        <div class="justify-content-center d-flex">
            <h3>Search Products</h3>
        </div>
        <div class="row p-3 d-flex justify-content-center">
            <input id="productid-search" type="number" class="col-sm-3 m-2 form-control"
                   placeholder="Search by ProductId">
            <input id="producttype-search" type="text" class="col-sm-3 m-2 form-control"
                   placeholder="Serach by Product Type">
            <input id="productname-search" type="text" class="col-sm-3 m-2 form-control"
                   placeholder="Serach by Product Name">
            <button onclick="searchProducts()" class="col-sm-2 m-2 btn btn-primary form-control">Find Products</button>
        </div>
    </div>

    <div id="results-field" class="shadow m-3 p-3">
        <h4>Product Details</h4>
        <form id="results-form">
            <div class="input-group form-group">
                <div class="input-group-prepend"><span class="input-group-text">Product Id</span></div>
                <input id="productid" name="product_name" type="text" class="form-control">
                <br>
            </div>
            <p id="update-check" class="text-danger">Required for updating the product</p>
            <div class="input-group form-group">
                <div class="input-group-prepend"><span class="input-group-text">Product Name</span></div>
                <input id="product_name" name="product_name" required type="text" class="form-control">
            </div>
            <div class="input-group form-group">
                <div class="input-group-prepend"><span class="input-group-text">Product Type</span></div>
                <input id="product_type" name="product_type" required type="text" class="form-control">
            </div>
            <div class="input-group form-group">
                <div class="input-group-prepend"><span class="input-group-text">Quantity</span></div>
                <input id="quantity" name="quantity" required type="text" class="form-control">
            </div>
            <div class="input-group form-group">
                <div class="input-group-prepend"><span class="input-group-text">Product Deleted Status</span></div>
                <input id="deleted" type="text" name="deleted" class="form-control">
            </div>
            <div class="input-group form-group">
                <div class="input-group-prepend"><span class="input-group-text">Product Description</span></div>
                <input id="product_description" required name="product_description" type="text" class="form-control">
            </div>
            <div class="input-group form-group">
                <div class="input-group-prepend"><span class="input-group-text">Product Image</span></div>
                <input id="product_image" required name="product_image" type="text" class="form-control">
            </div>
            <br>
            <div class="btn btn-primary" onclick="insertProduct()">Add Product</div>
            <button type="submit" class="btn btn-warning" onclick="updateProduct()">Update Product</button>
            <div class="btn btn-danger" onclick="deleteProduct()">Delete Product</div>
            <button type="reset" class="btn btn-secondary">Reset Data</button>

            <br>
            <p id="updateResult" class="text-success"></p>
        </form>
    </div>
    <br>
    <div id="multiple-results-list-holder" class="shadow p-3 m-3">
        <h3>Products</h3>
        <ul id="multiple-results-list" class="list-group">
        </ul>
    </div>
    <br>
    <div id="no-results-block" class="m-3 p-3 shadow justify-content-center">
        <h4>No Records Were Found</h4>
    </div>

    <script src="/js/manageproducts.js" defer></script>
@endsection
