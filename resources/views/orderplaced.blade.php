@extends("welcome")

@section("content")
    <div class="d-flex justify-content-center">

        <div id="placed-order-info-success" class="text-center shadow p-3 m-3
               @isset($failedOrderItems) d-none @endisset">
            <h2>Thank you for your Order</h2>
            <h3>Your Order has been placed !!</h3>

            <h3>Order Id - {{$orderId}}</h3>
            <br>
            Order will be Delivered in next 4 business days
            <br> <br>
            <a href="/" class="btn btn-success">Go Back To Shopping !!</a>
        </div>


        @isset($failedOrderItems)
            <div id="placed-order-failed" class="text-center shadow p-3">
                <h2>Sorry The Item Is Out of Stock !! </h2>
                <br>
                <div id="out-of-stock-product">
                    <h3>{{$failedOrderItems->product_name}}</h3> <br>
                    <h4>Out of Stock !!</h4>
                </div>
                <br> <br>
                <a href="/" class="btn btn-success">Go Back To Shopping !!</a>
            </div>
        @endisset
    </div>
@endsection
