@extends("welcome")

@section("content")
    <div id="payment-header" class="d-flex justify-content-center">
        <h1 class="shadow col-sm-6 text-center">Review Order - Payment Details</h1>
    </div>

    <br> <br>

    <div class="row">
        <div id="payment-form-holder" class="col m-3 p-3">
            <div class="shadow border border-dark col">
                <h3 class="col-sm-4 p-3 m-3">Card Details</h3>
                <form id="payment-form" class="p-3" method="post" action="/placeorder">
                    <div class="form-group input-group shadow">
                        <div class="input-group-prepend"><span class="input-group-text">Card Number</span></div>
                        <input type="text" class="form-control" name="card_number" required minlength="16"
                               placeholder="Looks like - 0123 4567 8910 1112">
                    </div>
                    <div class="form-group input-group shadow">
                        <div class="input-group-prepend"><span class="input-group-text">Name on Card</span></div>
                        <input type="text" class="form-control" name="card_name" required
                               placeholder="Looks like - John Doe">
                    </div>
                    <div class="form-group input-group shadow">
                        <div class="input-group-prepend"><span class="input-group-text">Security Code</span></div>
                        <input type="password" class="form-control" required name="card_code"
                               placeholder="Three or 4 digit code - 012(3)">
                    </div>
                    {{csrf_field()}}
                    <div class="row">
                        <div class="form-group input-group col-sm-4">
                            <div class="shadow input-group-prepend"><span class="input-group-text">Expire - Month</span>
                            </div>
                            <input type="text" class="form-control shadow" required name="card_month"
                                   placeholder="Month">
                        </div>
                        <div class="form-group input-group  col-sm-4">
                            <div class="input-group-prepend shadow"><span class="input-group-text">Expire - Year</span>
                            </div>
                            <input type="text" class="form-control shadow" required name="card_year" placeholder="Year">
                        </div>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-success shadow"> Place Order</button>
                    <button type="reset" class="btn btn-danger shadow"> Reset Details</button>
                </form>
            </div>

        </div>

        <div id="cart-total-holder" class="col m-3 p-3">
            <div id="cart-price-info" class="border border-dark shadow col">
                <h3 class="p-3 m-3">Cart Items Total </h3>
                @foreach($orderItemsList as $key => $cartItem)
                    <div id="price-info-{{$cartItem->product_name}}" class="p-3 m-3 shadow">
                        <div class="row">
                            <div class="col">
                                Item : {{$cartItem->product_name}}
                                <br>
                                Quantity : {{$cartItem->quantity}}
                                <br>
                                Price : ${{$cartItem->price}}
                            </div>
                            <div class="col">
                                <img class="float-right" src="/images/{{$cartItem->product_name}}.jpeg" width="75px"
                                     height="75px">
                            </div>
                        </div>
                    </div>
                @endforeach
                <div id="price-total" class="p-3 m-3 shadow border-success border ">
                    Total Items : {{$cartQuantity}}
                    <br>
                    Shipping Charges : 0.00
                    <br>
                    Taxes (8% of Cart Total) : ${{$cartTotal * 0.08}}
                    <br>
                    Total Cart Price : ${{$cartTotal * 1.08}}
                </div>
            </div>
        </div>
    </div>
@endsection
