@extends("welcome")

@section("content")
    <div id="address-header" class="d-flex justify-content-center">
        <h1 class="shadow col-sm-6 p-3 text-center">My Profile</h1>
    </div>
    <br>
    <div id="user-info-holder" class="p-3 m-3 shadow">
        <div class="col-sm-6">
            <h4>User Details</h4>
            UserName : {{$user->name}} <br>
            Email : {{$user->email}} <br>
            Member Since : {{$user->created_at}} <br>
        </div>
    </div>

    <div id="password-change-holder" class="p-3 m-3 shadow">
        <div class="col-sm-6">
            <h4>Manage Password</h4>
            <form id="change-password-form">
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text">Current Password</span></div>
                    <input class="form-control" type="password" name="current_password" required minlength="8"
                           id="currentPassword">
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text">New Password</span></div>
                    <input class="form-control" type="password" name="password" minlength="8" required id="password">
                </div>
                <div class="form-group input-group">
                    <div class="input-group-prepend"><span class="input-group-text">Confirm New Password</span></div>
                    <input class="form-control" type="password" minlength="8" name="password_confirmation" required
                           id="password_confirmation">
                </div>
                <p class="text-danger" id="no-match-text">Passwords did not match</p>
                <button class="btn btn-primary">
                    Change Password
                </button>
                <br>
                <p id="result-success" class="text-success">

                </p>
                <p id="result-fail" class="text-danger">

                </p>
            </form>
        </div>
    </div>

    <script src="/js/profile.js" defer></script>
@endsection
