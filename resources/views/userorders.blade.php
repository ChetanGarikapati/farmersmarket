@extends("welcome")

@section("content")
    <div id="address-header" class="d-flex justify-content-center">
        <h1 class="shadow col-sm-6 p-3 text-center">Your Orders</h1>
    </div>
    <br>

    <div id="user-orders-holder" class="m-3 p-3 col-sm-6">
        @isset($orders)
            @foreach($orders as $order)
                <div class="shadow p-3 m-3 col-sm-9 orders {{$order->order_status}}">
                    <div class="d-flex justify-content-center">
                        <h5>Order Id - {{$order->id}}</h5>
                    </div>
                    Order Item Count : {{$order->order_items_count}} <br>
                    Order Status : {{$order->order_status}} <br>
                    Order Total : ${{$order->order_total}}
                    <p>
                        Order Created On : <span class="text-primary">{{$order->created_at}}</span>
                    </p>
                    <a class="btn btn-primary" href="/myorders/details?orderid={{$order->id}}">View Order Details</a>
                </div>
            @endforeach
        @endisset

        @isset($order)
            @isset($orderItems)
                <div id="order-detail-holder" class="border-dark  border p-3 m-3">
                    <div class="d-flex m-3 p-3">
                        <h3>Order Details</h3>
                    </div>
                    @foreach($orderItems as $orderItem)
                        <div class="shadow p-3 m-3 col-sm-9">
                            <div class="row">
                                <div class="col">
                                    Order Item : {{$orderItem->product_name}}
                                    <br>
                                    Quantity : {{$orderItem->quantity}}
                                    <br>
                                    Price : $ {{$orderItem->price}}
                                    <br>
                                    Order Item Status : {{$orderItem->order_item_status}}
                                </div>
                                <div class="col">
                                    <img class="float-right" src="/images/{{$orderItem->product_name}}.jpeg"
                                         width="75px"
                                         height="75px">
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endisset
        @endisset
    </div>

    @isset($noorders)
        <div id="no-orders-holder" class="p-3 m-3">
            <div class="justify-content-center text-center">
                <h3>You don't have any orders yet!!</h3>
                <br/>
                <br/>
                <a href="/" class="btn shadow btn-lg btn-success">Go Back To Shopping !!</a>
            </div>
        </div>


    @endisset

    <div class="shadow p-3" id="return-to-shopping">
        <div class="justify-content-center text-center">
            <h3>Your cart is empty return to shopping !!</h3>
            <br/>
            <br/>
            <a href="/" class="btn shadow btn-lg btn-success">Go Back To Shopping</a>
        </div>
    </div>

    <script src="/js/userorders.js" defer></script>
@endsection
