<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name') }}</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src=" {{ asset('js/products.js') }}" defer></script>
    <link rel="stylesheet" href="{{ asset('css/app.css')  }}"/>
    <link rel="stylesheet" href="{{ asset('css/products.css')  }}"/>
</head>
<body>

<div id="main-navbar" class="shadow rounded sticky-top">
    @include('navbar')
</div>

<div id="filter-place-holder" class="shadow">
    @include("filter")
</div>

<div id="default-content" class="bg-white  m-3 p-3">
    @yield('content')
</div>

</body>
</html>
