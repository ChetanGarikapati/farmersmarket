<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AdminOrderMangementController;
use App\Http\Controllers\AdminProductManagementController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\MyOrdersController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\UserProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Shopper Flow Routes*/
Route::get('/', [App\Http\Controllers\DisplayProductsController::class, 'displayAllProducts']);
Route::get('/dashboard', [App\Http\Controllers\DisplayProductsController::class, 'displayAllProducts']);
Route::get("/cartInfo", [App\Http\Controllers\CartInfoController::class, 'getCartInfo']);
Route::post("/updateCart", [App\Http\Controllers\UpdateCart::class, 'saveCartInfo']);
Route::get("/showCart", [App\Http\Controllers\ShowCartController::class, 'displayCart']);

/*Checkout Flow Routes*/
Route::get("/checkout", [CheckoutController::class, 'processCheckoutFlow'])->middleware("auth");
Route::post("/checkout/saveAddress", [CheckoutController::class, 'saveAddress'])->middleware("auth");
Route::delete("/checkout/deleteAddress", [CheckoutController::class, 'deleteAddress'])->middleware("auth");

/*Payment and Order Finalization Routes*/
Route::get("/payment", [PaymentController::class, 'calculateOrderTotal'])->middleware("auth");
Route::post("/placeorder", [PaymentController::class, 'placeOrder'])->middleware("auth");

/*User Orders View*/
Route::get("/myorders", [MyOrdersController::class, 'displayUserOrders'])->middleware("auth");
Route::get("/myorders/details", [MyOrdersController::class, 'displayUserOrderDetails'])->middleware("auth");

/*User Profile*/
Route::get("/profile", [UserProfileController::class, "displayUserProfile"])->middleware("auth");
Route::post("/profile/updatepassword", [UserProfileController::class, "updateUserPassword"])->middleware("auth");

/*Admin Order Management Routes*/
Route::get("/admin", [AdminController::class, "validateUserAdminRole"])->middleware("auth");
Route::get("/admin/manageorders", [AdminOrderMangementController::class, "manageOrders"])->middleware("auth");
Route::get("/admin/updateorder", [AdminOrderMangementController::class, "updateOrderStatus"])->middleware("auth");

/*Admin User Management Routes*/
Route::get("/admin/manageuser", [App\Http\Controllers\AdminUserManagementController::class, 'getAdminSearchPage'])->middleware("auth");
Route::get("/admin/getusers", [App\Http\Controllers\AdminUserManagementController::class, 'getUsers'])->middleware("auth");
Route::get("/admin/getuserrole", [App\Http\Controllers\AdminUserManagementController::class, 'getUserRole'])->middleware("auth");
Route::post("/admin/updateuserrole", [App\Http\Controllers\AdminUserManagementController::class, 'updateUserRole'])->middleware("auth");

/*Admin Product Management Routes*/
Route::get("/admin/manageproducts/", [AdminProductManagementController::class, "manageProducts"]);
Route::get("/admin/manageproducts/getproduct", [AdminProductManagementController::class, "getProducts"]);
Route::post("/admin/manageproducts/updateproduct", [AdminProductManagementController::class, "update"]);
Route::post("/admin/manageproducts/saveproduct/", [AdminProductManagementController::class, "store"]);
Route::delete("/admin/manageproducts/deleteproduct/", [AdminProductManagementController::class, "delete"]);

Auth::routes();

